var mysql = require('mysql2')
const fs = require('fs')

const config = JSON.parse(fs.readFileSync('./config/audits.config.json'))

// Initiallising connection string
var pool = mysql.createPool({
  waitForConnections: true,
  connectionLimit: 10,
  host: process.env.HOST || config.mysql2.host,
  user: process.env.USER || config.mysql2.user,
  password: process.env.PASSWORD || config.mysql2.password,
  database: process.env.DB || config.mysql2.database,
  port: process.env.PORT || config.mysql2.port
})

/**
 * save the sales order informatiuon itno the MYSQL database
 *
 * @param {*} _jsonData - json data required for db sp
 */
const spSaveSalesOrderMYSQL = async (_jsonData) => {
  try {
    const sql = 'CALL usp_lq_sales_orders(?,?);'
    const option = 'saveSalesOrder'
    const promisePool = pool.promise()
    const [rows] = await promisePool.query(sql, [option, JSON.stringify(_jsonData)])
    return JSON.parse(JSON.stringify(rows[0]))
  } catch (ex) {
    console.log(`Error while connecting MySql database :- ${ex}`)
    throw ex
  }
}

/**
 * update the sales order information itno the MYSQL database
 *
 * @param {*} _jsonData - json data required for db sp
 */
const spUpdateSalesOrderMYSQL = async (_jsonData) => {
  try {
    const sql = 'CALL usp_lq_sales_orders(?,?);'
    const option = 'updateSalesOrder'
    const promisePool = pool.promise()
    const [rows] = await promisePool.query(sql, [option, JSON.stringify(_jsonData)])
    return JSON.parse(JSON.stringify(rows[0]))
  } catch (ex) {
    console.log(`Error while connecting MySql database :- ${ex}`)
    throw ex
  }
}

/**
 * get information about transport requisition from the MYSQL database
 *
 * @param {*} _jsonData - json data required for db sp
 */
const spGetSalesOrderMYSQL = async (_jsonData) => {
  try {
    const sql = 'CALL usp_lq_sales_orders(?,?);'
    const option = 'getSalesOrder'
    const promisePool = pool.promise()
    const [rows] = await promisePool.query(sql, [option, JSON.stringify(_jsonData)])
    return JSON.parse(JSON.stringify(rows[0]))
  } catch (ex) {
    console.log(`Error while connecting MySql database :- ${ex}`)
    throw ex
  }
}

/**
 * get details of an specific instrument
 *
 * @param {*} _jsonData - json data required for db sp
 */
const spGetInstrumentDetails = async (_jsonData) => {
  try {
    const sql = 'CALL api_queries(?,?);'
    const option = 'instrumento_detalle'
    const promisePool = pool.promise()
    const [rows] = await promisePool.query(sql, [option, JSON.stringify(_jsonData)])
    return JSON.parse(JSON.stringify(rows[0]))
  } catch (ex) {
    console.log(`Error while connecting MySql database :- ${ex}`)
    throw ex
  }
}

/**
 * get the list of all the instruments that are stored on the db
 *
 * @param {*} _jsonData - json data required for db sp
 */
const spGetInstruments = async (_jsonData) => {
  try {
    const sql = 'CALL api_queries(?,?);'
    const option = 'lista_instrumentos'
    const promisePool = pool.promise()
    const [rows] = await promisePool.query(sql, [option, JSON.stringify(_jsonData)])
    return JSON.parse(JSON.stringify(rows[0]))
  } catch (ex) {
    console.log(`Error while connecting MySql database :- ${ex}`)
    throw ex
  }
}

/**
 * get the list of all the users that are stored on the db
 *
 * @param {*} _jsonData - json data required for db sp
 */
const spGetUsers = async (_jsonData) => {
  try {
    const sql = 'CALL api_queries(?,?);'
    const option = 'lista_usuarios'
    const promisePool = pool.promise()
    const [rows] = await promisePool.query(sql, [option, JSON.stringify(_jsonData)])
    return JSON.parse(JSON.stringify(rows[0]))
  } catch (ex) {
    console.log(`Error while connecting MySql database :- ${ex}`)
    throw ex
  }
}

/**
 * get the list of all the customers that are stored on the db
 *
 * @param {*} _jsonData - json data required for db sp
 */
const spGetCustomers = async (_jsonData) => {
  try {
    const sql = 'CALL api_queries(?,?);'
    const option = 'lista_clientes'
    const promisePool = pool.promise()
    const [rows] = await promisePool.query(sql, [option, JSON.stringify(_jsonData)])
    return JSON.parse(JSON.stringify(rows[0]))
  } catch (ex) {
    console.log(`Error while connecting MySql database :- ${ex}`)
    throw ex
  }
}

/**
 * get the list of all the auditors that are stored on the db
 *
 * @param {*} _jsonData - json data required for db sp
 */
const spGetAuditors = async (_jsonData) => {
  try {
    const sql = 'CALL api_queries(?,?);'
    const option = 'lista_auditores'
    const promisePool = pool.promise()
    const [rows] = await promisePool.query(sql, [option, JSON.stringify(_jsonData)])
    return JSON.parse(JSON.stringify(rows[0]))
  } catch (ex) {
    console.log(`Error while connecting MySql database :- ${ex}`)
    throw ex
  }
}

/**
 * get the list of all the departments and the customer they belong to that are stored on the db
 *
 * @param {*} _jsonData - json data required for db sp
 */
const spGetdepartments = async (_jsonData) => {
  try {
    const sql = 'CALL api_queries(?,?);'
    const option = 'lista_departamentos'
    const promisePool = pool.promise()
    const [rows] = await promisePool.query(sql, [option, JSON.stringify(_jsonData)])
    return JSON.parse(JSON.stringify(rows[0]))
  } catch (ex) {
    console.log(`Error while connecting MySql database :- ${ex}`)
    throw ex
  }
}

/**
 * Insert an auditor on the db
 *
 * @param {*} _jsonData - json data required for db sp
 */
const spInsertAuditor = async (_jsonData) => {
  console.log(JSON.stringify(_jsonData))
  try {
    const sql = 'CALL api_queries(?,?);'
    const option = 'guardar_auditor'
    const promisePool = pool.promise()
    const [rows] = await promisePool.query(sql, [option, JSON.stringify(_jsonData)])
    return JSON.parse(JSON.stringify(rows[0]))
  } catch (ex) {
    console.log(`Error while connecting MySql database :- ${ex}`)
    throw ex
  }
}

/**
 * Inserts a customer on the db
 *
 * @param {*} _jsonData - json data required for db sp
 */
const spInsertCustomer = async (_jsonData) => {
  console.log(JSON.stringify(_jsonData))
  try {
    const sql = 'CALL api_queries(?,?);'
    const option = 'guardar_cliente'
    const promisePool = pool.promise()
    const [rows] = await promisePool.query(sql, [option, JSON.stringify(_jsonData)])
    return JSON.parse(JSON.stringify(rows[0]))
  } catch (ex) {
    console.log(`Error while connecting MySql database :- ${ex}`)
    throw ex
  }
}

/**
 * Inserts a department name [identifier] on the db
 *
 * @param {*} _jsonData - json data required for db sp
 */
const spInsertDepartmentName = async (_jsonData) => {
  console.log(JSON.stringify(_jsonData))
  try {
    const sql = 'CALL api_queries(?,?);'
    const option = 'guardar_nombreDepartamento'
    const promisePool = pool.promise()
    const [rows] = await promisePool.query(sql, [option, JSON.stringify(_jsonData)])
    return JSON.parse(JSON.stringify(rows[0]))
  } catch (ex) {
    console.log(`Error while connecting MySql database :- ${ex}`)
    throw ex
  }
}

/**
 * Inserts the department specific information such as email phone # etc.
 *
 * @param {*} _jsonData - json data required for db sp
 */
const spInsertDepartment = async (_jsonData) => {
  console.log(JSON.stringify(_jsonData))
  try {
    const sql = 'CALL api_queries(?,?);'
    const option = 'guardar_departamento'
    const promisePool = pool.promise()
    const [rows] = await promisePool.query(sql, [option, JSON.stringify(_jsonData)])
    return JSON.parse(JSON.stringify(rows[0]))
  } catch (ex) {
    console.log(`Error while connecting MySql database :- ${ex}`)
    throw ex
  }
}

/**
 * Inserts the user  information on the db.
 *
 * @param {*} _jsonData - json data required for db sp
 */
const spInsertUser = async (_jsonData) => {
  console.log(JSON.stringify(_jsonData))
  try {
    const sql = 'CALL api_queries(?,?);'
    const option = 'guardar_usuario'
    const promisePool = pool.promise()
    const [rows] = await promisePool.query(sql, [option, JSON.stringify(_jsonData)])
    return JSON.parse(JSON.stringify(rows[0]))
  } catch (ex) {
    console.log(`Error while connecting MySql database :- ${ex}`)
    throw ex
  }
}

/**
 * Inserts the user  information on the db.
 *
 * @param {*} _jsonData - json data required for db sp
 */
const spUpdateAuditor = async (_jsonData) => {
  console.log(JSON.stringify(_jsonData))
  try {
    const sql = 'CALL api_queries(?,?);'
    const option = 'actualizar_auditor'
    const promisePool = pool.promise()
    const [rows] = await promisePool.query(sql, [option, JSON.stringify(_jsonData)])
    return JSON.parse(JSON.stringify(rows[0]))
  } catch (ex) {
    console.log(`Error while connecting MySql database :- ${ex}`)
    throw ex
  }
}

/**
 * Inserts the user  information on the db.
 *
 * @param {*} _jsonData - json data required for db sp
 */
const spDeleteAuditor = async (_jsonData) => {
  console.log(JSON.stringify(_jsonData))
  try {
    const sql = 'CALL api_queries(?,?);'
    const option = 'eliminar_auditor'
    const promisePool = pool.promise()
    const [rows] = await promisePool.query(sql, [option, JSON.stringify(_jsonData)])
    return JSON.parse(JSON.stringify(rows[0]))
  } catch (ex) {
    console.log(`Error while connecting MySql database :- ${ex}`)
    throw ex
  }
}

/**
 * Inserts a customer on the db
 *
 * @param {*} _jsonData - json data required for db sp
 */
const spDeleteCustomer = async (_jsonData) => {
  console.log(JSON.stringify(_jsonData))
  try {
    const sql = 'CALL api_queries(?,?);'
    const option = 'eliminar_cliente'
    const promisePool = pool.promise()
    const [rows] = await promisePool.query(sql, [option, JSON.stringify(_jsonData)])
    return JSON.parse(JSON.stringify(rows[0]))
  } catch (ex) {
    console.log(`Error while connecting MySql database :- ${ex}`)
    throw ex
  }
}

const spUpdateCliente = async (_jsonData) => {
  console.log(JSON.stringify(_jsonData))
  try {
    const sql = 'CALL api_queries(?,?);'
    const option = 'actualizar_cliente'
    const promisePool = pool.promise()
    const [rows] = await promisePool.query(sql, [option, JSON.stringify(_jsonData)])
    return JSON.parse(JSON.stringify(rows[0]))
  } catch (ex) {
    console.log(`Error while connecting MySql database :- ${ex}`)
    throw ex
  }
}

const spUniqueUserName = async (_jsonData) => {
  try {
    const sql = 'CALL api_queries(?,?);'
    const option = 'unique_username'
    const promisePool = pool.promise()
    const [rows] = await promisePool.query(sql, [option, JSON.stringify(_jsonData)])
    return JSON.parse(JSON.stringify(rows[0]))
  } catch (ex) {
    console.log(`Error while connecting MySql database :- ${ex}`)
    throw ex
  }
}

const spUniqueEmail = async (_jsonData) => {
  try {
    const sql = 'CALL api_queries(?,?);'
    const option = 'unique_email'
    const promisePool = pool.promise()
    const [rows] = await promisePool.query(sql, [option, JSON.stringify(_jsonData)])
    return JSON.parse(JSON.stringify(rows[0]))
  } catch (ex) {
    console.log(`Error while connecting MySql database :- ${ex}`)
    throw ex
  }
}

const spGetLogin = async (_jsonData) => {
  try {
    const sql = 'CALL api_queries(?,?);'
    const option = 'login'
    const promisePool = pool.promise()
    const [rows] = await promisePool.query(sql, [option, JSON.stringify(_jsonData)])
    return JSON.parse(JSON.stringify(rows[0]))
  } catch (ex) {
    console.log(`Error while connecting MySql database :- ${ex}`)
    throw ex
  }
}

const spGetNADepartments = async (_jsonData) => {
  try {
    const sql = 'CALL api_queries(?,?);'
    const option = 'departamentos_no_asignados'
    const promisePool = pool.promise()
    const [rows] = await promisePool.query(sql, [option, JSON.stringify(_jsonData)])
    return JSON.parse(JSON.stringify(rows[0]))
  } catch (ex) {
    console.log(`Error while connecting MySql database :- ${ex}`)
    return ex
  }
}

const spPostInsertQuestionItems = async (_jsonData) => {
  try {
    const sql = 'CALL api_queries(?,?);'
    const option = 'insertar_reactivos'
    const promisePool = pool.promise()
    const [rows] = await promisePool.query(sql, [option, JSON.stringify(_jsonData)])
    return JSON.parse(JSON.stringify(rows[0]))
  } catch (ex) {
    console.log(`Error while connecting MySql database :- ${ex}`)
    return ex
  }
}

const spPostModifyInstrument = async (_jsonData) => {
  try {
    const sql = 'CALL api_queries(?,?);'
    const option = 'modificar_instrumento'
    const promisePool = pool.promise()
    const [rows] = await promisePool.query(sql, [option, JSON.stringify(_jsonData)])
    return JSON.parse(JSON.stringify(rows[0]))
  } catch (ex) {
    console.log(`Error while connecting MySql database :- ${ex}`)
    throw ex
  }
}

const spPostNewCopyInstrument = async (_jsonData) => {
  try {
    const sql = 'CALL api_queries(?,?);'
    const option = 'copiar_instrumento'
    const promisePool = pool.promise()
    const [rows] = await promisePool.query(sql, [option, JSON.stringify(_jsonData)])
    return JSON.parse(JSON.stringify(rows[0]))
  } catch (ex) {
    console.log(`Error while connecting MySql database :- ${ex}`)
    return ex
  }
}

// const spGetDepartmentNames = async (_jsonData) => {
//   try {
//     const sql = 'CALL api_queries(?,?);'
//     const option = 'lista_nombres_departamentos'
//     const promisePool = pool.promise()
//     const [rows] = await promisePool.query(sql, [option, JSON.stringify(_jsonData)])
//     return JSON.parse(JSON.stringify(rows[0]))
//   } catch (ex) {
//     console.log(`Error while connecting MySql database :- ${ex}`)
//     return ex
//   }
// }


const spUpdateDepartment = async (_jsonData) => {
  console.log(JSON.stringify(_jsonData))
  try {
    const sql = 'CALL api_queries(?,?);'
    const option = 'actualizar_departamento'
    const promisePool = pool.promise()
    const [rows] = await promisePool.query(sql, [option, JSON.stringify(_jsonData)])
    return JSON.parse(JSON.stringify(rows[0]))
  } catch (ex) {
    console.log(`Error while connecting MySql database :- ${ex}`)
    return ex
  }
}

const spGetInstrumentsAppliedFlags = async (_jsonData) => {
  try {
    const sql = 'CALL api_queries(?,?);'
    const option = 'instrument_flags'
    const promisePool = pool.promise()
    const [rows] = await promisePool.query(sql, [option, JSON.stringify(_jsonData)])
    return JSON.parse(JSON.stringify(rows[0]))
  } catch (ex) {
    console.log(`Error while connecting MySql database :- ${ex}`)
    return ex
  }
}

// const spPostNewCopyInstrument = async (_jsonData) => {
//   try {
//     const sql = 'CALL api_queries(?,?);'
//     const option = 'copiar_instrumento'
//     const promisePool = pool.promise()
//     const [rows] = await promisePool.query(sql, [option, JSON.stringify(_jsonData)])
//     return JSON.parse(JSON.stringify(rows[0]))
//   } catch (ex) {
//     console.log(`Error while connecting MySql database :- ${ex}`)
//     return ex
//   }
// }

const spGetDepartmentNames = async (_jsonData) => {
  try {
    const sql = 'CALL api_queries(?,?);'
    const option = 'lista_nombres_departamentos'
    const promisePool = pool.promise()
    const [rows] = await promisePool.query(sql, [option, JSON.stringify(_jsonData)])
    return JSON.parse(JSON.stringify(rows[0]))
  } catch (ex) {
    console.log(`Error while connecting MySql database :- ${ex}`)
    return ex
  }
}


// const spUpdateDepartment = async (_jsonData) => {
//   console.log(JSON.stringify(_jsonData))
//   try {
//     const sql = 'CALL api_queries(?,?);'
//     const option = 'actualizar_departamento'
//     const promisePool = pool.promise()
//     const [rows] = await promisePool.query(sql, [option, JSON.stringify(_jsonData)])
//     return JSON.parse(JSON.stringify(rows[0]))
//   } catch (ex) {
//     console.log(`Error while connecting MySql database :- ${ex}`)
//     return ex
//   }
// }

// const spGetInstrumentsAppliedFlags = async (_jsonData) => {
//   try {
//     const sql = 'CALL api_queries(?,?);'
//     const option = 'instrument_flags'
//     const promisePool = pool.promise()
//     const [rows] = await promisePool.query(sql, [option, JSON.stringify(_jsonData)])
//     return JSON.parse(JSON.stringify(rows[0]))
//   } catch (ex) {
//     console.log(`Error while connecting MySql database :- ${ex}`)
//     return ex
//   }
// }

const spPostInsertQuestions = async (_jsonData) => {
  try {
    const sql = 'CALL api_queries(?,?);'
    const option = 'insertar_preguntas'
    const promisePool = pool.promise()
    const [rows] = await promisePool.query(sql, [option, JSON.stringify(_jsonData)])
    return JSON.parse(JSON.stringify(rows[0]))
  } catch (ex) {
    console.log(`Error while connecting MySql database :- ${ex}`)
    throw ex
  }
}

const spPostModifyVersion = async (_jsonData) => {
  try {
    const sql = 'CALL api_queries(?,?);'
    const option = 'modificar_version_instrumento'
    const promisePool = pool.promise()
    const [rows] = await promisePool.query(sql, [option, JSON.stringify(_jsonData)])
    return JSON.parse(JSON.stringify(rows[0]))
  } catch (ex) {
    console.log(`Error while connecting MySql database :- ${ex}`)
    return ex
  }
}

const spGetEstados = async (_jsonData) => {
  console.log(JSON.stringify(_jsonData))
  try {
    const sql = 'CALL api_queries(?,?);'
    const option = 'lista_estados'
    const promisePool = pool.promise()
    const [rows] = await promisePool.query(sql, [option, JSON.stringify(_jsonData)])
    return JSON.parse(JSON.stringify(rows[0]))
  } catch (ex) {
    console.log(`Error while connecting MySql database :- ${ex}`)
    return ex
  }
}

// const spPostModifyVersion = async (_jsonData) => {
//   try {
//     const sql = 'CALL api_queries(?,?);'
//     const option = 'modificar_version_instrumento'
//     const promisePool = pool.promise()
//     const [rows] = await promisePool.query(sql, [option, JSON.stringify(_jsonData)])
//     return JSON.parse(JSON.stringify(rows[0]))
//   } catch (ex) {
//     console.log(`Error while connecting MySql database :- ${ex}`)
//     return ex
//   }
// }

// const spGetEstados = async (_jsonData) => {
//   console.log(JSON.stringify(_jsonData))
//   try {
//     const sql = 'CALL api_queries(?,?);'
//     const option = 'lista_estados'
//     const promisePool = pool.promise()
//     const [rows] = await promisePool.query(sql, [option, JSON.stringify(_jsonData)])
//     return JSON.parse(JSON.stringify(rows[0]))
//   } catch (ex) {
//     console.log(`Error while connecting MySql database :- ${ex}`)
//     return ex
//   }
// }

const spPostNewInstrument = async (_jsonData) => {
  try {
    const sql = 'CALL api_queries(?,?);'
    const option = 'insertar_nuevo_instrumento'
    const promisePool = pool.promise()
    const [rows] = await promisePool.query(sql, [option, JSON.stringify(_jsonData)])
    return JSON.parse(JSON.stringify(rows[0]))
  } catch (ex) {
    console.log(`Error while connecting MySql database :- ${ex}`)
    return ex
  }
}

const spGetCustomersAvailable = async (_jsonData) => {
  try {
    const sql = 'CALL api_queries(?,?);'
    const option = 'cliente_y_departamento_aplicable'
    const promisePool = pool.promise()
    const [rows] = await promisePool.query(sql, [option, JSON.stringify(_jsonData)])
    return JSON.parse(JSON.stringify(rows[0]))
  } catch (ex) {
    console.log(`Error while connecting MySql database :- ${ex}`)
    return ex
  }
}

const spGetCiudades = async (_jsonData) => {
  console.log(JSON.stringify(_jsonData))
  try {
    const sql = 'CALL api_queries(?,?);'
    const option = 'lista_ciudades'
    const promisePool = pool.promise()
    const [rows] = await promisePool.query(sql, [option, JSON.stringify(_jsonData)])
    return JSON.parse(JSON.stringify(rows[0]))
  } catch (ex) {
    console.log(`Error while connecting MySql database :- ${ex}`)
    return ex
  }
}

const spGetAuditorsAvailable = async (_jsonData) => {
  try {
    const sql = 'CALL api_queries(?,?);'
    const option = 'auditor_aplicable'
    const promisePool = pool.promise()
    const [rows] = await promisePool.query(sql, [option, JSON.stringify(_jsonData)])
    return JSON.parse(JSON.stringify(rows[0]))
  } catch (ex) {
    console.log(`Error while connecting MySql database :- ${ex}`)
    return ex
  }
}

// const spGetCustomersAvailable = async (_jsonData) => {
//   try {
//     const sql = 'CALL api_queries(?,?);'
//     const option = 'cliente_y_departamento_aplicable'
//     const promisePool = pool.promise()
//     const [rows] = await promisePool.query(sql, [option, JSON.stringify(_jsonData)])
//     return JSON.parse(JSON.stringify(rows[0]))
//   } catch (ex) {
//     console.log(`Error while connecting MySql database :- ${ex}`)
//     return ex
//   }
// }

// const spGetCiudades = async (_jsonData) => {
//   console.log(JSON.stringify(_jsonData))
//   try {
//     const sql = 'CALL api_queries(?,?);'
//     const option = 'lista_ciudades'
//     const promisePool = pool.promise()
//     const [rows] = await promisePool.query(sql, [option, JSON.stringify(_jsonData)])
//     return JSON.parse(JSON.stringify(rows[0]))
//   } catch (ex) {
//     console.log(`Error while connecting MySql database :- ${ex}`)
//     return ex
//   }
// }

// const spGetAuditorsAvailable = async (_jsonData) => {
//   try {
//     const sql = 'CALL api_queries(?,?);'
//     const option = 'auditor_aplicable'
//     const promisePool = pool.promise()
//     const [rows] = await promisePool.query(sql, [option, JSON.stringify(_jsonData)])
//     return JSON.parse(JSON.stringify(rows[0]))
//   } catch (ex) {
//     console.log(`Error while connecting MySql database :- ${ex}`)
//     throw ex
//   }
// }

const spPostNewVersionInstrument = async (_jsonData) => {
  try {
    const sql = 'CALL api_queries(?,?);'
    const option = 'nueva_version_instrumento'
    const promisePool = pool.promise()
    const [rows] = await promisePool.query(sql, [option, JSON.stringify(_jsonData)])
    return JSON.parse(JSON.stringify(rows[0]))
  } catch (ex) {
    console.log(`Error while connecting MySql database :- ${ex}`)
    throw ex
  }
}

const spGetResultados = async (_jsonData) => {
  try {
    const sql = 'CALL api_queries(?,?);'
    const option = 'resultados'
    const promisePool = pool.promise()
    const [rows] = await promisePool.query(sql, [option, JSON.stringify(_jsonData)])
    return JSON.parse(JSON.stringify(rows[0]))
  } catch (ex) {
    console.log(`Error while connecting MySql database :- ${ex}`)
    throw ex
  }
}

const spPostAssignAudit = async (_jsonData) => {
  try {
    const sql = 'CALL api_queries(?,?);'
    const option = 'asignar_auditoria'
    const promisePool = pool.promise()
    const [rows] = await promisePool.query(sql, [option, JSON.stringify(_jsonData)])
    return JSON.parse(JSON.stringify(rows[0]))
  } catch (ex) {
    console.log(`Error while connecting MySql database :- ${ex}`)
    return ex
  }
}

const spGetAuditsPending = async (_jsonData) => {
  try {
    const sql = 'CALL api_queries(?,?);'
    const option = 'lista_pendientes_audit'
    const promisePool = pool.promise()
    const [rows] = await promisePool.query(sql, [option, JSON.stringify(_jsonData)])
    return JSON.parse(JSON.stringify(rows[0]))
  } catch (ex) {
    console.log(`Error while connecting MySql database :- ${ex}`)
    return ex
  }
}

const spGetAuditQuestionItems = async (_jsonData) => {
  try {
    const sql = 'CALL api_queries(?,?);'
    const option = 'audit_question_items'
    const promisePool = pool.promise()
    const [rows] = await promisePool.query(sql, [option, JSON.stringify(_jsonData)])
    return JSON.parse(JSON.stringify(rows[0]))
  } catch (ex) {
    console.log(`Error while connecting MySql database :- ${ex}`)
    return ex
  }
}

const spPostSaveAudit = async (_jsonData) => {
  try {
    const sql = 'CALL api_queries(?,?);'
    const option = 'salvar_auditoria'
    const promisePool = pool.promise()
    const [rows] = await promisePool.query(sql, [option, JSON.stringify(_jsonData)])
    return JSON.parse(JSON.stringify(rows[0]))
  } catch (ex) {
    console.log(`Error while connecting MySql database :- ${ex}`)
    return ex
  }
}

module.exports = {
  spSaveSalesOrderMYSQL,
  spUpdateSalesOrderMYSQL,
  spGetSalesOrderMYSQL,
  spGetInstrumentDetails,
  spGetInstruments,
  spPostModifyInstrument,
  spPostInsertQuestions,
  spPostNewCopyInstrument,
  spPostNewVersionInstrument,
  spPostInsertQuestionItems,
  spGetUsers,
  spGetCustomers,
  spGetAuditors,
  spGetdepartments,
  spInsertAuditor,
  spInsertCustomer,
  spInsertDepartmentName,
  spInsertDepartment,
  spInsertUser,
  spUpdateAuditor,
  spDeleteAuditor,
  spDeleteCustomer,
  spUpdateCliente,
  spGetDepartmentNames,
  spGetInstrumentsAppliedFlags,
  spPostModifyVersion,
  spPostNewInstrument,
  spGetCustomersAvailable,
  spGetAuditorsAvailable,
  spUniqueUserName,
  spUniqueEmail,
  spGetLogin,
  spGetNADepartments,
  spUpdateDepartment,
  spGetEstados,
  spGetCiudades,
  spGetResultados,
  spPostAssignAudit,
  spGetAuditsPending,
  spGetAuditQuestionItems,
  spPostSaveAudit
}
