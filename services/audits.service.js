const {
  spPostModifyInstrument, spPostInsertQuestions,
  spPostNewCopyInstrument, spPostInsertQuestionItems, spGetInstrumentsAppliedFlags, spPostModifyVersion,
  spSaveSalesOrderMYSQL, spUpdateSalesOrderMYSQL, spGetSalesOrderMYSQL, spGetInstrumentDetails, spGetInstruments, spGetUsers, spGetCustomers, spGetAuditors,
  spGetdepartments, spInsertAuditor, spInsertCustomer, spInsertDepartmentName, spInsertDepartment, spInsertUser, spUpdateAuditor, spDeleteAuditor, spUpdateCliente,
  spDeleteCustomer, spUniqueUserName, spUniqueEmail, spGetLogin, spGetDepartmentNames, spPostNewInstrument, spGetCustomersAvailable, spGetAuditorsAvailable,
  spPostNewVersionInstrument, spGetNADepartments, spUpdateDepartment,
  spGetEstados, spGetCiudades, spGetResultados, spPostAssignAudit, spGetAuditsPending, spGetAuditQuestionItems, spPostSaveAudit
} = require('../db')

const saveSalesOrderMYSQL = async (_jsonData) => {
  try {
    return await spSaveSalesOrderMYSQL(_jsonData)
  } catch (ex) {
    console.log(ex)
    return ex
  }
}

const updateSalesOrderMYSQL = async (_jsonDat) => {
  try {
    return await spUpdateSalesOrderMYSQL(_jsonData)
  } catch (ex) {
    console.log(ex)
    return ex
  }
}

const getSalesOrderMYSQL = async (_jsonDat) => {
  try {
    return await spGetSalesOrderMYSQL(_jsonData)
  } catch (ex) {
    console.log(ex)
    return ex
  }
}

const getInstrumentDetails = async (_jsonData) => {
  try {
    return await spGetInstrumentDetails(_jsonData)
  } catch (ex) {
    console.log(ex)
    return ex
  }
}

const getInstruments = async (_jsonData) => {
  try {
    return await spGetInstruments(_jsonData)
  } catch (ex) {
    console.log(ex)
    return ex
  }
}

const getInstrumentsAppliedFlags = async (_jsonData) => {
  try {
    return await spGetInstrumentsAppliedFlags(_jsonData)
  } catch (ex) {
    console.log(ex)
    return ex
  }
}

const postModifyInstrument = async (_jsonData) => {
  try {
    return await spPostModifyInstrument(_jsonData)
  } catch (ex) {
    console.log(ex)
    return ex
  }
}

const getUsers = async (_jsonData) => {
  try {
    return await spGetUsers(_jsonData)
  } catch (ex) {
    console.log(ex)
    return ex
  }
}

const postInsertQuestions = async (_jsonData) => {
  try {
    return await spPostInsertQuestions(_jsonData)
  } catch (ex) {
    console.log(ex)
    return ex
  }
}

const getCustomers = async (_jsonData) => {
  try {
    return await spGetCustomers(_jsonData)
  } catch (ex) {
    console.log(ex)
    return ex
  }
}

const postModifyVersion = async (_jsonData) => {
  try {
    return await spPostModifyVersion(_jsonData)
  } catch (ex) {
    console.log(ex)
    return ex
  }
}

const postNewCopyInstrument = async (_jsonData) => {
  try {
    return await spPostNewCopyInstrument(_jsonData)
  } catch (ex) {
    console.log(ex)
    return ex
  }
}

const postNewVersionInstrument = async (_jsonData) => {
  try {
    return await spPostNewVersionInstrument(_jsonData)
  } catch (ex) {
    console.log(ex)
    return ex
  }
}

const getAuditors = async (_jsonData) => {
  try {
    return await spGetAuditors(_jsonData)
  } catch (ex) {
    console.log(ex)
    return ex
  }
}

const postInsertQuestionItems = async (_jsonData) => {
  try {
    return await spPostInsertQuestionItems(_jsonData)
  } catch (ex) {
    console.log(ex)
    return ex
  }
}

const getDepartments = async (_jsonData) => {
  try {
    return await spGetdepartments(_jsonData)
  } catch (ex) {
    console.log(ex)
    return ex
  }
}

const insertAuditor = async (_jsonData) => {
  try {
    return await spInsertAuditor(_jsonData)
  } catch (ex) {
    console.log(ex)
    return ex
  }
}

const insertCustomer = async (_jsonData) => {
  try {
    return await spInsertCustomer(_jsonData)
  } catch (ex) {
    console.log(ex)
    return ex
  }
}

const insertDepartmentName = async (_jsonData) => {
  try {
    return await spInsertDepartmentName(_jsonData)
  } catch (ex) {
    console.log(ex)
    return ex
  }
}

const insertDepartment = async (_jsonData) => {
  try {
    return await spInsertDepartment(_jsonData)
  } catch (ex) {
    console.log(ex)
    return ex
  }
}

const insertUser = async (_jsonData) => {
  try {
    return await spInsertUser(_jsonData)
  } catch (ex) {
    console.log(ex)
    return ex
  }
}

const updateAuditor = async (_jsonData) => {
  try {
    return await spUpdateAuditor(_jsonData)
  } catch (ex) {
    console.log(ex)
    return ex
  }
}

const removeAuditor = async (_jsonData) => {
  try {
    return await spDeleteAuditor(_jsonData)
  } catch (ex) {
    console.log(ex)
    return ex
  }
}

const removeCustomer = async (_jsonData) => {
  try {
    return await spDeleteCustomer(_jsonData)
  } catch (ex) {
    console.log(ex)
    return ex
  }
}

const updateCliente = async (_jsonData) => {
  try {
    return await spUpdateCliente(_jsonData)
  } catch (ex) {
    console.log(ex)
    return ex
  }
}

const uniqueUserName = async (_jsonData) => {
  try {
    return await spUniqueUserName(_jsonData)
  } catch (ex) {
    console.log(ex)
    return ex
  }
}

const uniqueEmail = async (_jsonData) => {
  try {
    return await spUniqueEmail(_jsonData)
  } catch (ex) {
    console.log(ex)
    return ex
  }
}

const getLogin = async (_jsonData) => {
  try {
    return await spGetLogin(_jsonData)
  } catch (ex) {
    console.log(ex)
    return ex
  }
}

const getDepartmentNames = async (_jsonData) => {
  try {
    return await spGetDepartmentNames(_jsonData)
  } catch (ex) {
    console.log(ex)
    return ex
  }
}

const getNADepartments = async (_jsonData) => {
  try {
    return await spGetNADepartments(_jsonData)
  } catch (ex) {
    console.log(ex)
    return ex
  }
}

const postNewInstrument = async (_jsonData) => {
  try {
    return await spPostNewInstrument(_jsonData)
  } catch (ex) {
    console.log(ex)
    return ex
  }
}

const updateDepartment = async (_jsonData) => {
  try {
    return await spUpdateDepartment(_jsonData)
  } catch (ex) {
    console.log(ex)
    return ex
  }
}

const getCustomersAvailable = async (_jsonData) => {
  try {
    return await spGetCustomersAvailable(_jsonData)
  } catch (ex) {
    console.log(ex)
    return ex
  }
}

const getEstados = async (_jsonData) => {
  try {
    return await spGetEstados(_jsonData)
  } catch (ex) {
    console.log(ex)
    return ex
  }
}

const getAuditorsAvailable = async (_jsonData) => {
  try {
    return await spGetAuditorsAvailable(_jsonData)
  } catch (ex) {
    console.log(ex)
    return ex
  }
}

const getCiudades = async (_jsonData) => {
  try {
    return await spGetCiudades(_jsonData)
  } catch (ex) {
    console.log(ex)
    return ex
  }
}

const getResultados = async (_jsonData) => {
  try {
    return await spGetResultados(_jsonData)
  } catch (ex) {
    console.log(ex)
    return ex
  }
}

const postAssignAudit = async (_jsonData) => {
  try {
    return await spPostAssignAudit(_jsonData)
  } catch (ex) {
    console.log(ex)
    return ex
  }
}

const getAuditsPending = async (_jsonData) => {
  try {
    return await spGetAuditsPending(_jsonData)
  } catch (ex) {
    console.log(ex)
    return ex
  }
}

const getAuditQuestionItems = async (_jsonData) => {
  try {
    return await spGetAuditQuestionItems(_jsonData)
  } catch (ex) {
    console.log(ex)
    return ex
  }
}

const postSaveAudit = async (_jsonData) => {
  try {
    return await spPostSaveAudit(_jsonData)
  } catch (ex) {
    console.log(ex)
    return ex
  }
}

module.exports = {
  saveSalesOrderMYSQL,
  updateSalesOrderMYSQL,
  getSalesOrderMYSQL,
  getInstrumentDetails,
  getInstruments,
  postModifyInstrument,
  postInsertQuestions,
  postNewCopyInstrument,
  postNewVersionInstrument,
  postInsertQuestionItems,
  getUsers,
  getCustomers,
  getAuditors,
  getDepartments,
  insertAuditor,
  insertCustomer,
  insertDepartmentName,
  insertDepartment,
  insertUser,
  updateAuditor,
  removeAuditor,
  removeCustomer,
  updateCliente,
  uniqueUserName,
  uniqueEmail,
  getLogin,
  getDepartmentNames,
  getInstrumentsAppliedFlags,
  postModifyVersion,
  postNewInstrument,
  getCustomersAvailable,
  getAuditorsAvailable,
  getNADepartments,
  updateDepartment,
  getEstados,
  getCiudades,
  getResultados,
  postAssignAudit,
  getAuditsPending,
  getAuditQuestionItems,
  postSaveAudit
}
