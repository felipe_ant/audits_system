# TMTC Leg Quarter API

This project will contain the Audits system backend.

## Getting Started

Clone it from Gitlab repository

```
Give examples
```

### Prerequisites

```
Give examples
```

### Break down into the project

Routes:

```
Handle the HTTP requests that hits the API and route them to appropriate controller(s)
```

Controllers:

```
Take request object, pull out data from request, validate, then send to service(s)
```

Services:

```
Contain the logic that encapsulates your business requirements, calls your classes, utils, data access layer or models, calls API’s external to the Node application.
```

Data Access Layer or Modles:

```
Contains your logic for accessing persistent data, stored procedures or Models when it's used as part of an ORM.
```

Middlewares:

```
[WIP] It will contain the Authetications / access with okta 
```

Classes:

```
Contains part of the business requirements encapulates in methods
```

Config:

```
Contains all the configurations files used in all the project
```

Utils:

```
Contains all common logic functions that are not necessarily specific to your business logic or domain, or even a REST API in general
```

**Note:** All the folder architecture was based on the following source pages:

* [Project structure](https://www.coreycleary.me/project-structure-for-an-express-rest-api-when-there-is-no-standard-way/) - for an Express REST API when there is no “standard way”
* [Separate Controllers from Services](https://www.coreycleary.me/why-should-you-separate-controllers-from-services-in-node-rest-apis/)
* [How I structure my REST APIs](https://dev.to/lars124/how-i-structure-my-rest-apis-11k4)


## Deployment

Add additional notes about how to deploy this on a live system

## Built With

[WIP]

## Versioning

[WIP]

## Release History

* 1.0
    * [WIP]

## Authors

Felipe Rodriguez –

Antonio Mares  - jmaresarrietagmail.com

## Contributing

1. Fork it (<https://gitlab.tyson.com/IT-FFHI/CWM/leg-quarter-process-api.git>)
2. Create your feature branch (`git checkout -b audits_system/<featureBranch>`)
3. Commit your changes (`git commit -m '[ADD|DEL|REF|FIX|UPD|WIP][example.sql] - Small Description '`)
4. Push to the branch (`git push origin audits_system/<featureBranch>`)
5. Create a new Pull Request

![](https://www.agweb.com/assets/1/6/500x500_70929e00-scftf.png?4263043)

## Acknowledgments
