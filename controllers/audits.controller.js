const {
  updatePOMSSQL, updatePOMYSQL, getPOInfo, getSoShID, saveSalesOrderMSSQL, saveSalesOrderMYSQL,
  postModifyInstrument, postInsertQuestions, postModifyVersion, postNewInstrument,
  postNewCopyInstrument, postInsertQuestionItems, getDepartmentNames, getInstrumentsAppliedFlags,
  updateSalesOrderMSSQL, updateSalesOrderMYSQL, getSalesOrderMYSQL, getInstrumentDetails, getInstruments, getUsers, getCustomers, getAuditors, getDepartments,
  insertAuditor, insertCustomer, insertDepartmentName, insertDepartment, insertUser, updateAuditor, removeAuditor, removeCustomer, updateCliente,
  uniqueUserName, uniqueEmail, getLogin, getCustomersAvailable, getAuditorsAvailable,
  getResultados, postNewVersionInstrument, getNADepartments, updateDepartment, getEstados, getCiudades, postAssignAudit,
  getAuditsPending, getAuditQuestionItems, postSaveAudit,
} = require('../services')

const { TmtcEmail } = require('../utils')
const email = new TmtcEmail()
const { returnPDF } = require('../utils')
var pdf = require('html-pdf')

// const updatePOStatus = async (req, res) => {
//   try {
//     let to, subject, bodyTitle, bodyMsg, lang
//     to = ', daniel.torresgutierrez@tyson.com, hector.camacho@tyson.com, jose.mares@tyson.com,'
//     lang = 'spanish'
//     // first we need to get the PO Folio incremental from MSSQL DataBase
//     const _jsonData = req.body
//     // Update in MySQL
//     const resultMYSQL = await updatePOMYSQL(_jsonData)
//     // Update in MSSQL
//     const resultMSSQL = await updatePOMSSQL(_jsonData)
//     // console.log('MSSQL response', resultMSSQL)
//     let to2 = ''; let _aut = ''
//     resultMSSQL.forEach(element => {
//       to2 += `${element.email} ,`
//       if (element.idWorkflow === 'AUT') _aut = element.shortName
//     })
//     // send the corresponding email
//     let st, _st
//     _jsonData.AuthPO.status === 'A' ? st = 'ya se encuentra aprobada' : st = 'ha sido rechazada'
//     _jsonData.AuthPO.status === 'A' ? _st = 'Aprobada' : _st = 'Rechazada'

//     subject = `Notificacion de Prueba de Orden de Compra ${_st}`
//     bodyTitle = subject
//     bodyMsg = `La orden de compra con el Folio: <strong> &nbsp${_jsonData.AuthPO.poId} </strong> ${st} por <strong>${_aut}</strong><br/>
//                  Por Favor, inicia sesion dentro del sistema y verifica la informacion<br/>`
//     // replace the hardcoded to for to2 declaration when it moves to qa
//     email.sendNotification(to, subject, bodyTitle, bodyMsg, lang)
//     res.status(201).json({ status: 'success', message: `The PO has been proccessed successfully with ID ${_jsonData.AuthPO.poId}` })
//   } catch (ex) {
//     res.status(500).send({ error: `${ex}` })
//     console.log(ex)
//   }
// }

// const POPDF = async (req, res) => {
//   try {
//     const _jsonData = req.query
//     _jsonData.poId = 6019
//     const result = await getPOInfo(_jsonData)
//     /** generating the pdf file on buffer fashion */
//     const _file = returnPDF(result)
//     return pdf.create(_file).toBuffer(function (err, buffer) {
//       res.status(200).send(buffer)
//     })
//   } catch (ex) {
//     res.status(500).send({ error: `${ex}` })
//     console.log(ex)
//   }
// }

// const saveSalesOrder = async (req, res) => {
//   try {
//     let to, subject, bodyTitle, bodyMsg, lang
//     to = 'daniel.torresgutierrez@tyson.com, hector.camacho@tyson.com, jose.mares@tyson.com'
//     lang = 'spanish'
//     // first we need to get the PO Folio incremental from MSSQL DataBase
//     const _jsonData = req.body
//     const result = await getSoShID(_jsonData)
//     // fill the SOID position in the _jsonData object since it will be required for the SQL/MYSQL procedures for insert
//     //_jsonData.purchaseOrder.poId = `${result[0].POID}`

//     // saving sql and mysql results in case theyre needed for future features
//     // insert into SQL
//     const resultSQL = await saveSalesOrderMSSQL(_jsonData)
//     // insert into MYSQL
//     const resultMYSQL = await saveSalesOrderMYSQL(_jsonData)
//     // if (resultSQL && resultMYSQL && resultUpdatePOID) res.status(201).send({ etsito: 'el registro se guardo exitosamente' })
//     subject = 'Orden De Compra Pendiente Por Autorizar'
//     bodyTitle = subject
//     bodyMsg = `La orden de compra con el Folio: <strong> &nbsp${_jsonData.purchaseOrder.poId} </strong> ha sido generada y esta pendiente para ser autorizada<br/>
//                  Por Favor, inicia sesion dentro del sistema y verifica la informacion<br/>`
//     email.sendNotification(to, subject, bodyTitle, bodyMsg, lang)
//     res.status(201).json({ status: 'success', message: `The PO has been created successfully with ID ${_jsonData.purchaseOrder.poId}` })
//   } catch (ex) {
//     res.status(500).send({ error: `${ex}` })
//     console.log(ex)
//   }
// }

// const updateSalesOrder = async (req, res) => {
//   try {
//     let to, subject, bodyTitle, bodyMsg, lang
//     to = 'daniel.torresgutierrez@tyson.com, hector.camacho@tyson.com, jose.mares@tyson.com'
//     lang = 'spanish'
//     // first we need to get the PO Folio incremental from MSSQL DataBase
//     const _jsonData = req.body
//     // saving sql and mysql results in case theyre needed for future features
//     // insert into SQL
//     const resultSQL = await updateSalesOrderMSSQL(_jsonData)
//     // insert into MYSQL
//     const resultMYSQL = await updateSalesOrderMYSQL(_jsonData)
//     // if (resultSQL && resultMYSQL && resultUpdatePOID) res.status(201).send({ etsito: 'el registro se guardo exitosamente' })
//     subject = 'Orden De Compra Pendiente Por Autorizar'
//     bodyTitle = subject
//     bodyMsg = `La orden de compra con el Folio: <strong> &nbsp${_jsonData.purchaseOrder.poId} </strong> ha sido generada y esta pendiente para ser autorizada<br/>
//                  Por Favor, inicia sesion dentro del sistema y verifica la informacion<br/>`
//     email.sendNotification(to, subject, bodyTitle, bodyMsg, lang)
//     res.status(201).json({ status: 'success', message: `The PO has been created successfully with ID ${_jsonData.purchaseOrder.poId}` })
//   } catch (ex) {
//     res.status(500).send({ error: `${ex}` })
//     console.log(ex)
//   }
// }

const salesOrderMYSQL = async (req, res) => {
  try {
    const _jsonData = req.query
    const result = await getSalesOrderMYSQL(_jsonData)
    res.status(200).send(result)
  } catch {
    res.status(500).send({ error: `${ex}` })
    console.log(ex)
  }
}

const instrumentDetails = async (req, res) => {
  try {
    const _jsonData = req.query
    const result = await getInstrumentDetails(_jsonData)
    res.status(200).send(result)
  } catch {
    res.status(500).send({ error: `${ex}` })
    console.log(ex)
  }
}

const instruments = async (req, res) => {
  try {
    const _jsonData = req.query
    const result = await getInstruments(_jsonData)
    res.status(200).send(result)
  } catch {
    res.status(500).send({ error: `${ex}` })
    console.log(ex)
  }
}

const instrumentsAppliedFlags = async (req, res) => {
  try {
    const _jsonData = req.query
    const result = await getInstrumentsAppliedFlags(_jsonData)
    res.status(200).send(result)
  } catch {
    res.status(500).send({ error: `${ex}` })
    console.log(ex)
  }
}

const modifyInstrument = async (req, res) => {
  try {
    const _jsonData = req.body.params
    const result = await postModifyInstrument(_jsonData)
    res.status(200).send(result)
  } catch {
    res.status(500).send({ error: `${ex}` })
    console.log(ex)
  }
}

const modifyInstrumentContent = async (req, res) => {
  try {
    const _jsonData = req.body.params
    var _instrumento = { "id": "", "nombre": "" }
    _instrumento.id = _jsonData[0].instrumento_id
    _instrumento.nombre = _jsonData[0].nombre
    const result = await postModifyVersion(_instrumento)
    var _preguntasArray = []
    _preguntasArray = Array.from(new Set(_jsonData.map(obj => obj.pregunta_id)))
      .map(pregunta_id => {
        return {
          pregunta: _jsonData.find(obj => obj.pregunta_id === pregunta_id).pregunta,
          "tipo": _jsonData.find(obj => obj.pregunta_id === pregunta_id).tipo,
          "fk_instrumento": _instrumento.id
        };
      });
    const result2 = await postInsertQuestions(_preguntasArray)
    var _reactivosArray = []
    for (let i = 0; i < result2.length; i++) {
      for (let i2 = 0; i2 < _jsonData.length; i2++) {
        if (result2[i].pregunta == _jsonData[i2].pregunta) {
          let _reactivo = {
            "reactivo": _jsonData[i2].reactivo,
            "valor": _jsonData[i2].valor,
            "fk_pregunta": result2[i].id
          }
          _reactivosArray.push(_reactivo)
        }
      }
    }
    const result3 = await postInsertQuestionItems(_reactivosArray)
    res.status(200).send(result3)
  } catch (ex) {
    res.status(500).send({ error: `${ex}` })
    console.log(ex)
  }
}

const newCopyInstrument = async (req, res) => {
  try {
    const _jsonData = req.body.params
    var _instrumento = { "id": "", "nombre": "" }
    _instrumento.id = _jsonData[0].instrumento_id
    _instrumento.nombre = _jsonData[0].nombre
    //buscar instrumento en auditorias, si no esta ahi, borrarlo, en cualquier caso es insertar uno nuevo
    //antes de borrar, guardar la version para sumarle uno y asignarselo al nuevo instrumento a insertar
    const result = await postNewCopyInstrument(_instrumento)
    var _newInstrumentId = result[0].id
    //var _newInstrumentId = 6
    var _preguntasArray = []
    _preguntasArray = Array.from(new Set(_jsonData.map(obj => obj.pregunta_id)))
      .map(pregunta_id => {
        return {
          pregunta: _jsonData.find(obj => obj.pregunta_id === pregunta_id).pregunta,
          "tipo": _jsonData.find(obj => obj.pregunta_id === pregunta_id).tipo,
          "fk_instrumento": _instrumento.id
        };
      });
    const result2 = await postInsertQuestions(_preguntasArray)
    var _reactivosArray = []
    for (let i = 0; i < result2.length; i++) {
      for (let i2 = 0; i2 < _jsonData.length; i2++) {
        if (result2[i].pregunta == _jsonData[i2].pregunta) {
          let _reactivo = {
            "reactivo": _jsonData[i2].reactivo,
            "valor": _jsonData[i2].valor,
            "fk_pregunta": result2[i].id
          }
          _reactivosArray.push(_reactivo)
        }
      }
    }
    const result3 = await postInsertQuestionItems(_reactivosArray)
    res.status(200).send(result3)
  } catch (ex) {
    res.status(500).send({ error: `${ex}` })
    console.log(ex)
  }
}

const departmentNames = async (req, res) => {
  try {
    const _jsonData = req.query
    const result = await getDepartmentNames(_jsonData)
    res.status(200).send(result)
  } catch {
    res.status(500).send({ error: `${ex}` })
    console.log(ex)
  }
}


const newInstrument = async (req, res) => {
  try {
    const _jsonData = req.body.params
    const result = await postNewInstrument(_jsonData)
    res.status(200).send(result)
  } catch {
    res.status(500).send({ error: `${ex}` })
    console.log(ex)
  }
}

const assignAudit = async (req, res) => {
  try {
    const _jsonData = req.body.params
    const result = await postAssignAudit(_jsonData)
    res.status(200).send(result)
  } catch {
    res.status(500).send({ error: `${ex}` })
    console.log(ex)
  }
}

const customersAvailable = async (req, res) => {
  try {
    const _jsonData = req.query
    const result = await getCustomersAvailable(_jsonData)
    res.status(200).send(result)
  } catch {
    res.status(500).send({ error: `${ex}` })
    console.log(ex)
  }
}

const auditorsAvailable = async (req, res) => {
  try {
    const _jsonData = req.query
    const result = await getAuditorsAvailable(_jsonData)
    res.status(200).send(result)
  } catch {
    res.status(500).send({ error: `${ex}` })
    console.log(ex)
  }
}


// const newInstrument = async (req, res) => {
//   try {
//     const _jsonData = req.body.params
//     const result = await postNewInstrument(_jsonData)
//     res.status(200).send(result)
//   } catch {
//     res.status(500).send({ error: `${ex}` })
//     console.log(ex)
//   }
// }

// const customersAvailable = async (req, res) => {
//   try {
//     const _jsonData = req.query
//     const result = await getCustomersAvailable(_jsonData)
//     res.status(200).send(result)
//   } catch {
//     res.status(500).send({ error: `${ex}` })
//     console.log(ex)
//   }
// }

// const auditorsAvailable = async (req, res) => {
//   try {
//     const _jsonData = req.query
//     const result = await getAuditorsAvailable(_jsonData)
//     res.status(200).send(result)
//   } catch {
//     res.status(500).send({ error: `${ex}` })
//     console.log(ex)
//   }
// }

const users = async (req, res) => {
  try {
    const _jsonData = req.query
    const result = await getUsers(_jsonData)
    res.status(200).send(result)
  } catch {
    res.status(500).send({ error: `${ex}` })
    console.log(ex)
  }
}

const customers = async (req, res) => {
  try {
    const _jsonData = req.query
    const result = await getCustomers(_jsonData)
    res.status(200).send(result)
  } catch {
    res.status(500).send({ error: `${ex}` })
    console.log(ex)
  }
}

const auditors = async (req, res) => {
  try {
    const _jsonData = req.query
    const result = await getAuditors(_jsonData)
    res.status(200).send(result)
  } catch {
    res.status(500).send({ error: `${ex}` })
    console.log(ex)
  }
}

const departments = async (req, res) => {
  try {
    const _jsonData = req.query
    const result = await getDepartments(_jsonData)
    res.status(200).send(result)
  } catch {
    res.status(500).send({ error: `${ex}` })
    console.log(ex)
  }
}

const saveAuditor = async (req, res) => {
  try {
    const _jsonData = req.body
    //console.log(JSON.stringify(_jsonData))
    const result = await insertAuditor(_jsonData)
    res.status(200).send(result)
  } catch {
    res.status(500).send({ error: `${ex}` })
    console.log(ex)
  }
}

const saveCustomer = async (req, res) => {
  try {
    const _jsonData = req.body
    //console.log(JSON.stringify(_jsonData))
    const result = await insertCustomer(_jsonData)
    res.status(200).send(result)
  } catch {
    res.status(500).send({ error: `${ex}` })
    console.log(ex)
  }
}

const saveDepartmentName = async (req, res) => {
  try {
    const _jsonData = req.body
    //console.log(JSON.stringify(_jsonData))
    const result = await insertDepartmentName(_jsonData)
    res.status(200).send(result)
  } catch {
    res.status(500).send({ error: `${ex}` })
    console.log(ex)
  }
}

const saveDepartment = async (req, res) => {
  try {
    const _jsonData = req.body
    //console.log(JSON.stringify(_jsonData))
    const result = await insertDepartment(_jsonData)
    res.status(200).send(result)
  } catch {
    res.status(500).send({ error: `${ex}` })
    console.log(ex)
  }
}

const saveUser = async (req, res) => {
  try {
    const _jsonData = req.body
    //console.log(JSON.stringify(_jsonData))
    const result = await insertUser(_jsonData)
    res.status(200).send(result)
  } catch {
    res.status(500).send({ error: `${ex}` })
    console.log(ex)
  }
}

const modifyAuditor = async (req, res) => {
  try {
    const _jsonData = req.body
    const result = await updateAuditor(_jsonData)
    res.status(200).send(result)
  } catch {
    res.status(500).send({ error: `${ex}` })
    console.log(ex)
  }
}

const deleteAuditor = async (req, res) => {
  try {
    const _jsonData = req.body
    const result = await removeAuditor(_jsonData)
    res.status(200).send(result)
  } catch {
    res.status(500).send({ error: `${ex}` })
    console.log(ex)
  }
}

const deleteCustomer = async (req, res) => {
  try {
    const _jsonData = req.body
    const result = await removeCustomer(_jsonData)
    res.status(200).send(result)
  } catch {
    res.status(500).send({ error: `${ex}` })
    console.log(ex)
  }
}


const actualizarCliente = async (req, res) => {
  try {
    const _jsonData = req.body
    const result = await updateCliente(_jsonData)
    res.status(200).send(result)
  } catch {
    res.status(500).send({ error: `${ex}` })
    console.log(ex)
  }
}

const getUniqueUserName = async (req, res) => {
  try {
    const _jsonData = req.query
    const result = await uniqueUserName(_jsonData)
    res.status(200).send(result)
  } catch {
    res.status(500).send({ error: `${ex}` })
    console.log(ex)
  }
}

const getUniqueEmail = async (req, res) => {
  try {
    const _jsonData = req.query
    const result = await uniqueEmail(_jsonData)
    res.status(200).send(result)
  } catch {
    res.status(500).send({ error: `${ex}` })
    console.log(ex)
  }
}

const login = async (req, res) => {
  try {
    console.log(req.query)
    const _jsonData = req.query
    const result = await getLogin(_jsonData)
    //console.log('result', result)
    res.status(200).send(result)
  } catch {
    res.status(500).send({ error: `${ex}` })
    console.log(ex)
  }
}

const nADepartments = async (req, res) => {
  try {
    // console.log(req.query)
    const _jsonData = req.query
    const result = await getNADepartments(_jsonData)
    console.log('result', result)
    res.status(200).send(result)
  } catch {
    res.status(500).send({ error: `${ex}` })
    console.log(ex)
  }
}

const modifyDepartment = async (req, res) => {
  try {
    console.log(req.query)
    const _jsonData = req.body
    const result = await updateDepartment(_jsonData)
    console.log('result', result)
    res.status(200).send(result)
  } catch {
    res.status(500).send({ error: `${ex}` })
    console.log(ex)
  }
}

const estados = async (req, res) => {
  try {
    console.log(req.query)
    const _jsonData = req.body
    const result = await getEstados(_jsonData)
    console.log('result', result)
    res.status(200).send(result)
  } catch {
    res.status(500).send({ error: `${ex}` })
    console.log(ex)
  }
}

const ciudades = async (req, res) => {
  try {
    console.log(req.query)
    const _jsonData = req.query
    const result = await getCiudades(_jsonData)
    console.log('result', result)
    res.status(200).send(result)
  } catch {
    res.status(500).send({ error: `${ex}` })
    console.log(ex)
  }
}

const resultados = async (req, res) => {
  try {
    console.log(req.query)
    const _jsonData = req.query
    const result = await getResultados(_jsonData)
    console.log('result', result)
    res.status(200).send(result)
  } catch {
    res.status(500).send({ error: `${ex}` })
    console.log(ex)
  }
}

const auditsPending = async (req, res) => {
  try {
    const _jsonData = req.query
    const result = await getAuditsPending(_jsonData)
    res.status(200).send(result)
  } catch {
    res.status(500).send({ error: `${ex}` })
    console.log(ex)
  }
}

const auditQuestionItems = async (req, res) => {
  try {
    const _jsonData = req.query
    const result = await getAuditQuestionItems(_jsonData)
    res.status(200).send(result)
  } catch {
    res.status(500).send({ error: `${ex}` })
    console.log(ex)
  }
}

const saveAudit = async (req, res) => {
  try {
    const _jsonData = req.body.params
    const result = await postSaveAudit(_jsonData)
    res.status(200).send(result)
  } catch {
    res.status(500).send({ error: `${ex}` })
    console.log(ex)
  }
}

module.exports = {
  salesOrderMYSQL,
  instrumentDetails,
  instruments,
  modifyInstrumentContent,
  modifyInstrument,
  instrumentsAppliedFlags,
  newCopyInstrument,
  departmentNames,
  users,
  customers,
  auditors,
  departments,
  saveAuditor,
  saveCustomer,
  saveDepartmentName,
  saveDepartment,
  saveUser,
  modifyAuditor,
  deleteAuditor,
  deleteCustomer,
  actualizarCliente,
  getUniqueUserName,
  getUniqueEmail,
  login,
  newInstrument,
  customersAvailable,
  auditorsAvailable,
  nADepartments,
  modifyDepartment,
  estados,
  ciudades,
  resultados,
  assignAudit,
  auditsPending,
  auditQuestionItems,
  saveAudit
}
