const {
    instrumentDetails, modifyInstrumentContent, newCopyInstrument, departmentNames, instruments, users, customers, modifyInstrument, instrumentsAppliedFlags,
    auditors, departments, saveAuditor, saveCustomer, saveDepartmentName, saveDepartment, saveUser,
    modifyAuditor, deleteAuditor, deleteCustomer, actualizarCliente, getUniqueUserName, getUniqueEmail, login, resultados,
    modifyDepartment, estados, ciudades, newVersionInstrument, assignAudit, auditsPending, auditQuestionItems, saveAudit,
    newInstrument, customersAvailable, auditorsAvailable, nADepartments
} = require('../controllers')

const express = require('express')
const auditsRouter = express.Router()

auditsRouter.get('/instrument/questions', (req, res) => {
    instrumentDetails(req, res)
})

auditsRouter.get('/instruments', (req, res) => {
    instruments(req, res)
})

auditsRouter.get('/instruments/appliedflags', (req, res) => {
    instrumentsAppliedFlags(req, res)
})

auditsRouter.post('/instrument/assignaudit', (req, res) => {
    assignAudit(req, res)
})

auditsRouter.get('/pending', (req, res) => {
    auditsPending(req, res)
})

auditsRouter.get('/audit/question/items', (req, res) => {
    auditQuestionItems(req, res)
})

auditsRouter.post('/audit/save', (req, res) => {
    saveAudit(req, res)
})

auditsRouter.post('/instrument/save/modify', (req, res) => {
    modifyInstrument(req, res)
})

auditsRouter.post('/instrument/questions/save/modify', (req, res) => {
    modifyInstrumentContent(req, res)
})

auditsRouter.post('/instrument/questions/save/newcopy', (req, res) => {
    newCopyInstrument(req, res)
})

auditsRouter.post('/instruments/create', (req, res) => {
    newInstrument(req, res)
})

auditsRouter.get('/instrument/customers', (req, res) => {
    customersAvailable(req, res)
})

auditsRouter.get('/instrument/auditors', (req, res) => {
    auditorsAvailable(req, res)
})

auditsRouter.get('/departmentnames', (req, res) => {
    departmentNames(req, res)
})

auditsRouter.get('/users', (req, res) => {
    users(req, res)
})

auditsRouter.get('/customers', (req, res) => {
    customers(req, res)
})

auditsRouter.get('/auditors', (req, res) => {
    auditors(req, res)
})

auditsRouter.get('/departments', (req, res) => {
    departments(req, res)
})

auditsRouter.post('/auditor', (req, res) => {
    saveAuditor(req, res)
})

auditsRouter.post('/customer', (req, res) => {
    saveCustomer(req, res)
})

auditsRouter.post('/departmentName', (req, res) => {
    saveDepartmentName(req, res)
})

auditsRouter.post('/department', (req, res) => {
    saveDepartment(req, res)
})

auditsRouter.post('/user', (req, res) => {
    saveUser(req, res)
})

auditsRouter.put('/auditor', (req, res) => {
    modifyAuditor(req, res)
})

auditsRouter.delete('/auditor', (req, res) => {
    deleteAuditor(req, res)
})

auditsRouter.delete('/customer', (req, res) => {
    deleteCustomer(req, res)
})

auditsRouter.put('/customer', (req, res) => {
    actualizarCliente(req, res)
})

auditsRouter.get('/uniqueUsername', (req, res) => {
    getUniqueUserName(req, res)
})

auditsRouter.get('/uniqueEmail', (req, res) => {
    getUniqueEmail(req, res)
})

auditsRouter.get('/login', (req, res) => {
    login(req, res)
})

auditsRouter.get('/nadepartments', (req, res) => {
    nADepartments(req, res)
})

auditsRouter.put('/department', (req, res) => {
    modifyDepartment(req, res)
})

auditsRouter.get('/estados', (req, res) => {
    estados(req, res)
})

auditsRouter.get('/ciudades', (req, res) => {
    ciudades(req, res)
})

auditsRouter.get('/resultados', (req, res) => {
    resultados(req, res)
})

module.exports = auditsRouter
