// Initiallising node modules
const express = require('express')
const bodyParser = require('body-parser')
const { auditsRouter } = require('./routes')
var app = express()

// Body Parser Middleware
app.use(bodyParser.json())

// CORS Middleware
app.use((req, res, next) => {
  // Enabling CORS
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT,DELETE')
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, contentType,Content-Type, Accept, Authorization')
  next()
})

app.use('/api', auditsRouter)

// Setting up server
const server = app.listen(process.env.PORT || 8080, () => {
  const port = server.address().port
  console.log(`App now running on port ${port}`)
})
