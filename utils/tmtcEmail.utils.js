const os = require('os')
const _sendmail = require('sendmail')({
  logger: {
    debug: console.log,
    info: console.info,
    warn: console.warn,
    error: console.error
  },
  // set to false if yous want to debug the email send proccess
  silent: true,
  smtpPort: 25, // Default: 25
  smtpHost: 'exchgw.tyson.com' // Default: -1 - extra smtp host after resolveMX
})

/**
 * TMTC Email Class for Vacations Request
 */
class TmtcEmail {
  /**
     * Send Email Notification
     * @param {*} subject
     * @param {*} bodyTitle
     * @param {*} bodyMessage
     * @param {*} lang
     */
  async sendNotification (to, subject, bodyTitle, bodyMessage, lang) {
    const body = this.bodyMessage('', '', bodyTitle, bodyMessage, lang)
    _sendmail({
      from: 'no-reply@tyson.com',
      // to: 'daniel.torresgutierrez@tyson.com, hector.camacho@tyson.com, jose.mares@tyson.com',
      to: to,
      // bcc: 'daniel.torresgutierrez@tyson.com, hector.camacho@tyson.com',
      subject: subject,
      html: body // 'Mail of test sendmail (ﾉ◕ヮ◕)ﾉ*:･ﾟ✧ ✧ﾟ･: *ヽ(◕ヮ◕ヽ)'
    }, function (err, reply) {
      console.log(err && err.stack)
      console.dir(reply)
    })
  }

  async errorNotification (service, errMsg) {
    const body = this.bodyMessage('', '', service, errMsg, '')
    _sendmail({
      from: 'no-reply@tyson.com',
      to: 'daniel.torresgutierrez@tyson.com, hector.camacho@tyson.com ',
      subject: 'Error',
      html: 'Mail of test sendmail (ﾉ◕ヮ◕)ﾉ*:･ﾟ✧ ✧ﾟ･: *ヽ(◕ヮ◕ヽ)'
    }, function (err, reply) {
      console.log(err && err.stack)
      console.dir(reply)
    })
  }

  bodyMessage (app, id, title, body, lang) {
    let servidor, resultMsg
    const route = `http://${os.hostname()}:900/menu/vacaciones/autorizar`

    if (lang === 'EN') { // English
      resultMsg = `<html>
                     <body>
                         <table width="80%" align="center" style="border-collapse: collapse">
                             <tr>
                                 <td style="background-color: #002554" width="20">
                                     <img src="https://storage.googleapis.com/ffhi_logos/logo_200.png" alt="Notifications" width="180" height="100"/>
                                 </td>
                                 <td style="background-color: #002554; font-size: 20px; color: #fff; text-align: left" width="80%">
                                     <strong>${title}</strong>            
                                 </td>
                             </tr>
                             <tr>
                                 <td colspan="2" style="text-align: center padding: 0 0 20px background: #EDF3F9 font-size: 15px">
                                 <br>${body}<br>
                                 <br>Please, log into TMTC Web System and validate the information.<br><br>
                                 <a href=${route} style="text-decoration: none color: #FFF background-color: #002554 border: solid #002554 border-width: 10px 20px line-height: 2em font-weight: bold text-align: center cursor: pointer display: inline-block border-radius: 5px">
                                    Log in</a>
                             </td>
                         </table>
                     </body>
                  </html>`
    } else { // Spanish
      resultMsg = `<html>
                     <body>
                         <table width="80%" align="center" style="border-collapse: collapse">
                             <tr>
                                 <td style="background-color: #002554" width="20">
                                     <img src="https://storage.googleapis.com/ffhi_logos/logo_200.png" alt="Envio de bitacora" width="180" height="100"/>
                                 </td>
                                 <td style="background-color: #002554; font-size: 20px; color: #fff; text-align: left" width="80%">
                                     <strong>${title}</strong>            
                                 </td>
                             </tr>
                             <tr>
                                 <td colspan="2" style="background-color: #EDF3F9; text-align: center; padding: 0 0 20px; font-size: 15px;">
                                 <br>${body}<br>
                                 <br>
                             </td>
                         </table>
                     </body>
                  </html>`
    }
    return resultMsg
  }
}

module.exports = {
  TmtcEmail
}
