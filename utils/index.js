const { TmtcEmail } = require('./tmtcEmail.utils')
const { returnPDF } = require('./formatoOc')

module.exports = { TmtcEmail, returnPDF }
