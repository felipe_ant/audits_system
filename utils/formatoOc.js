const returnPDF = (_jsonData) => {
  // Extract the specific position from array of result from sql...
  const purchaseOrder = JSON.parse(_jsonData[0].Result)
  // extract PO from the array...
  const _data = purchaseOrder.purchaseOrder[0]
  const _pData = _data.products
  console.log('po structure', _data)
  let _pRows = ''
  const headerHint = `<tr>
  <th class="pumpkin" style='width: 10%; border-bottom: solid 2px black; padding: 0px;'>ITEM.</th>
  <th class="pumpkin" style='width: 36%; border-bottom: solid 2px black; padding: 0px;'>DESCRIPTION</th>
  <th class="pumpkin" style='width: 9.5%; border-bottom: solid 2px black; padding: 0px;'>CASE</th>
  <th class="pumpkin" style='width: 9.5%; border-bottom: solid 2px black; padding: 0px;'>LBS</th>
  <th class="pumpkin" style='width: 9.5%; border-bottom: solid 2px black; padding: 0px;'>U.M</th>
  <th class="pumpkin" style='width: 9.5%; border-bottom: solid 2px black; padding: 0px;'>PRICE</th>
  <th class="pumpkin" style='width: 16%; border-bottom: solid 2px black; padding: 0px;'>TOTAL</th>
</tr>`
  _pData.forEach(element => {
    _pRows += `
  <tr>
    <td style='width: 10%; border-bottom: solid 2px black; padding: 0px;'>${element.item}</td>
    <td style='width: 36%; border-bottom: solid 2px black; padding: 0px;'>${element.itemDescription}</td>
    <td style='width: 9.5%; border-bottom: solid 2px black; padding: 0px;'>${element.totalBoxes}</td>
    <td style='width: 9.5%; border-bottom: solid 2px black; padding: 0px;'>${element.totalWeight}</td>
    <td style='width: 9.5%; border-bottom: solid 2px black; padding: 0px;'>${element.measureUnit}</td>
    <td style='width: 9.5%; border-bottom: solid 2px black; padding: 0px;'>${element.poUnitPrice}</td>
    <td style='width: 16%; border-bottom: solid 2px black; padding: 0px;'>${element.amount}</td>
  </tr>`
  })
  return `<style type="text/css">
  table {
      vertical-align: top;
  }
  tr {
      vertical-align: top
  }
  td {
      vertical-align: top;
  }
  .pumpkin {
      background:#D8D8D8;
      padding: 4px 4px 4px 4px;
      color:black;
      font-weight:bold;
      font-size:12px;
      border-collapse: collapse;
  }
  table.page_footer {
      width: 100%;
      border: none;
      background-color: white;
      padding: 2mm;
      border-collapse:collapse;
      border: none;
  }
  #tbl_articulos {
      border-collapse: collapse;
      width: 100%;
  }
  #tbl_articulos td, #tbl_articulos th {
      border: 1px solid #000000;
      padding: 5px;
  }
  #tbl_articulos th {
      padding-top: 4px;
      padding-bottom: 4px;
      background-color: #D8D8D8;
      color: black;
  } */
</style>
<page style="font-size: 9pt; font-family: Arial, Helvetica, sans-serif;" >
<div id="generalInfo">
  <table>
    <tbody>
      <tr>
        <td>
          <img src="../assets/tyson_foods.png" style="max-width:100px;" />
        </td>
        <td style="width:507px; text-align:center;">
          <h4 style="color:black;">PURCHASE ORDER</h4>
          <h4 style="color:black; margin-top: -10px;">TYSON MEXICO TRADING COMPANY S DE RL DE CV</h4>
        </td>
        <td>
          <table cellspacing="0" style="width: 100%; border: solid 1px#000000; padding:0;">
            <thead>
              <tr>
                  <th class="pumpkin" style="width: 50%; text-align:center; border-bottom: solid 1px black;"> <strong>PO</strong> </th>          
                  <th class="pumpkin" style="width: 50%; text-align:center; border-bottom: solid 1px black; border-left: solid 1px black;"><strong>DATE</strong></th>          
              </tr>
            </thead>
            <tbody>
              <tr>
                <td style="text-align:center;">
                  <strong>${_data.poId}</strong>
                </td>
                <td style='border-left: 1px solid black;'>
                ${_data.shipDate}
                </td>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>
    </tbody>
  </table>
</div>
<br>
<div id="wrap" style="position:relative;">
  <div id="left" style="margin-right:250px;">
    <table cellspacing="0" style="width: 100%; border: solid 1px#000000; text-align: center; padding:0;">
      <tr>
        <th class="pumpkin" style='padding: 2px; width: 100px; text-align: left' >APLICANT</th>
        <th style='padding: 2px; border-left: solid 1px black;'>${_data.aplicant}</th>
      </tr>
    </table>
  </div>
  <div id="right" style="position:absolute;width:200px;right:0;top:0;">
    <table cellspacing="0" style="width: 100%; border: solid 1px#000000; text-align: center; padding:0;">
      <tr>
        <th class="pumpkin" style='padding: 2px;'>EFFECTIVE</th>
        <th style='padding: 2px; border-left: solid 1px black;'>${_data.startDate} to ${_data.dueDate}</th>
      </tr>
    </table>
  </div>
</div>
<br>
<div id="supplierInfo">
  <table cellspacing="0" style="width: 100%; border: solid 1px#000000; text-align: center; padding:0;">
    <tr>
      <th class="pumpkin" style="width: 100%; text-align:center;border-bottom: solid 1px black;">SUPPLIER:</th>          
    </tr>
    <tr>
      <td style="text-align:left">
        <table>
          <tr>
              <td>
                <label id="lbl_Prov"> 302270</label> -
                <label id="lbl_Nom_Prov"> TYSON FOODS INC</label>
              </td>
          </tr>
          <!-- <tr><td></td><td></td></tr>
          <tr><td></td><td></td></tr> -->
          <tr style="padding-bottom: 25px;"> <!-- Direccion -->
            <td></td style="width: 160px;">
            <td>
              <label> 71-0225165</label>
            </td>
          </tr>
          <!-- <tr><td></td><td></td></tr>
          <tr><td></td><td></td></tr> -->
          <tr> <!-- Colonia -->
            <td></td style="width: 160px;">
            <td>
              <label> 2200 W DON TYSON PKWY</label>
            </td>
          </tr>
          <!-- <tr><td></td><td></td></tr>
          <tr><td></td><td></td></tr> -->
          <tr> <!-- Ciudad -->
            <td></td style="width: 160px;">
            <td>
              <label> WASHINGTON, SPRINGDALE,AR,US C.P. 72762</label>
            </td>
          </tr>
          <!-- <tr><td></td><td></td></tr>
          <tr><td></td><td></td></tr> -->
          <tr> <!-- Rfc -->
            <td></td style="width: 160px;">
            <td>
              <label> TEL:000-000-0000</label>
              <label></label>
            </td>                     
          </tr>
          <tr> <!-- Rfc -->
            <td></td style="width: 160px;">
            <td>
              <label> CORREO:</label>
              <label></label>
            </td>                     
          </tr>
          <tr> <!-- Rfc -->
            <td></td style="width: 160px;">
            <td>
              <label></label>
              <label></label>
              <label></label>
              <label></label>
              <label></label>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</div>
<br>
<div id="nofityBillto">
  <table cellspacing="0" style="width: 100%; border: solid 1px#000000; padding:0;">
    <thead>
      <tr>
          <th class="pumpkin" style="width: 50%; text-align:center; border-bottom: solid 1px black;"> NOTIFY PARTY / SHIP TO</th>          
          <th class="pumpkin" style="width: 50%; text-align:center; border-bottom: solid 1px black; border-left: solid 1px black;"> BILL TO / FINAL DESTINATION</th>          
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>
          Continental Forwarding, Service, Inc.
        </td>
        <td style='border-left: solid 1px black;'>
          TMT141030820
        </td>
      </tr>
      <tr>
        <td>
          23852 Mines Rd.
        </td>
        <td style='border-left: solid 1px black;'>
          Tyson Mexico Trading Company
        </td>
      </tr>
      <tr>
        <td>
          Laredo, Tx. 78045 USA
        </td>
        <td style='border-left: solid 1px black;'>
          Paseo de la Reforma No. 115 Cuarto Piso,
        </td>
      </tr>
      <tr>
        <td>
          Contacto: Juan José de Léon
        </td>
        <td style='border-left: solid 1px black;'>
          Colonia Lomas de Chapultepec, Sección I
        </td>
      </tr>
      <tr>
        <td>
          Tel. 956-52-14-02
        </td>
        <td style='border-left: solid 1px black;'>
          Mexico City, C.P. 11000
        </td>
      </tr>
    </tbody>
  </table>
</div>

<br>

<div id="deliveryInstructions">
  <table cellspacing="0" style="width: 100%; border: solid 1px#000000; padding:0;">
    <thead>
      <tr>
        <th class="pumpkin" colspan="2" style="width: 100%; text-align:center; border-bottom: solid 1px black; padding: 2px;"> DELIVERY INSTRUCTIONS</th>
      </tr>
      <tr>
        <th style="text-align:center; border-bottom: solid 1px black; border-right: solid 1px black; padding: 2px;"><strong>ADDRESS</strong> </th>
        <th style="text-align:center; border-bottom: solid 1px black;"><strong>DELIVERY DATE</strong></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td></td>
        <td style="text-align:center; border-bottom: solid 1px black; border-left: solid 1px black; ; padding: 2px;"> ${_data.startDate} </td>
      </tr>
      <tr>
        <td>${_data.warehouseId} Domicilio:${_data.address}</td>
        <td style="text-align:center; border-bottom: solid 1px black; border-left: solid 1px black; padding: 2px;"><strong>SHIPPING DATE</strong></td>
      </tr>
      <tr>
        <td></td>
        <td style="text-align:center; border-left: solid 1px black; padding: 2px;"> ${_data.startDate} </td>
      </tr>
    </tbody>
  </table>
</div>

<br>

<div id="tblProducts">
  <table cellspacing="0" id="tbl_articulos" style="width: 99%; border: solid 1px#000000; text-align: center; padding:0;">
    <tr>
      <th class="pumpkin" style='width: 10%; border-bottom: solid 2px black; padding: 0px;'>ITEM.</th>
      <th class="pumpkin" style='width: 36%; border-bottom: solid 2px black; padding: 0px;'>DESCRIPTION</th>
      <th class="pumpkin" style='width: 9.5%; border-bottom: solid 2px black; padding: 0px;'>CASE</th>
      <th class="pumpkin" style='width: 9.5%; border-bottom: solid 2px black; padding: 0px;'>LBS</th>
      <th class="pumpkin" style='width: 9.5%; border-bottom: solid 2px black; padding: 0px;'>U.M</th>
      <th class="pumpkin" style='width: 9.5%; border-bottom: solid 2px black; padding: 0px;'>PRICE</th>
      <th class="pumpkin" style='width: 16%; border-bottom: solid 2px black; padding: 0px;'>TOTAL</th>
    </tr>
    ${_pRows}
    <tr>
      <th class="pumpkin" colspan="2" style='width: 10%;'>TOTAL</th>
      <th class="pumpkin" style='width: 10%;'>950</th>
      <th class="pumpkin" style='width: 10%;'>38,000.000</th>
      <th class="pumpkin" colspan="2" style='width: 10%;'></th>
      <th class="pumpkin" style='width: 10%;'>10,830.00</th>
    </tr>
    <tr>
      <td colspan="6" style="text-align:right; border: 0px; border-right: solid 1px black;"><strong>TOTAL</strong></td>
      <td style=""><strong>${_pData.amountTotal}</strong></td>
    </tr>
    <tr>
      <td colspan="6" style="text-align:right; border: 0px; border-right: solid 1px black"></td>
      <td style=" border-top: solid 0px black;"><strong>${_data.currency}</strong></td>
    </tr>
  </table>
</div>

<br>

<div id="comments">
  <table cellspacing="0" style="width: 100%; border: solid 1px#000000;">
    <thead>
      <tr>
        <th class="pumpkin" style='width: 100%; text-align:center; border-bottom: solid 1px black;'>COMMENTS</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td style='padding: 15px 15px 15px 2px;'>${_data.comments}</td>
      </tr>
    </tbody>
  </table>
</div>

<br>

<div id="incoterm">
  <table cellspacing="0" style="width: 100%; border: solid 1px#000000; padding:0;">
    <thead>
      <tr>
        <th class="pumpkin" style='width: 100%; text-align:center; border-bottom: solid 1px black;'>INCOTERM</th>
      </tr>
    </thead>
    <tbody>
      <tr style='margin-top:50px'>
        <td style='padding: 6px 6px 6px 2px;'> ${_data.incoterm}</td>
      </tr>
    </tbody>
  </table>
</div>
<br>
<div>
  <small>
    By accepting this purchase order, Supplier acknowledges the validity and enforceability of its terms and conditions and shall deliver the product(s) on
    the agreed date. Late delivery by Supplier will allow the Company to accept or reject the subject matter merchandise or product.
  </small>
</div>
<page_footer>
  <div style="text-align:center;">
      <label>FOR TYSON MEXICO TRADING COMPANY USE ONLY - CONFIDENTIAL</label>
  </div>
</page_footer>
</page>
<page style= "font-family: arial">
<page_footer>
  <div style="text-align:center;">
      <label>FOR TYSON MEXICO TRADING COMPANY USE ONLY - CONFIDENTIAL</label>
  </div>
</page_footer>

<div style="width: 100%; text-align: center;"> GENERAL CONDITIONS </div>
<p style="width: 100%; text-align: justify;">
  Company and the Supplier agree to hire the services object of this agreement or service order (herein after the “Service Order”),
  according to the following General Conditions, in addition to the terms duly set forth by the Parties at the obverse of this
  document/page
</p>

<table style="width: 100%;">
    <tr style="width: 100%;">
        <td style="width: 50%;">
                <p style="text-align: justify; padding-right: 1mm">
                1. In the case of an executed Contract with the
                description of and/or about the same services of the Service
                Order, both documents shall be interpreted consistently and in if
                in the case of a conflict between their terms, the Contract shall
                prevail over theService Order.<br>
                2. The signature, execution or confirmation of the
                Supplier by its legal representative or responsible personnel
                stated at the obverse of this page, shall mean the Supplier’s
                understanding and duly acceptance of these General
                Conditions.<br>
                3. The delivery of the goods and/or the
                materialization of the services object of this Service Order shall
                be understood as executed by the Supplier in a way that
                Company can receive all its inherent benefits without any
                limitation.<br>
                4. Supplier shall perform its duties of this Service
                Order, in a professional and diligent way, according to the
                applicable customs and trade practices and according as well,
                to the quality of the services in this document with Company
                agreed.<br>
                5. The property of the goods shall be effectively
                transferred to Company, until the moment that the goods have
                already been accepted by Company.<br>
                6. If the Supplier delivers goods or provides services
                exceeding the terms and provisions set forth at the obverse of
                this page, Company shall not be obligated to pay to the Supplier
                for that excessive goods or services, except by the regarding
                prior and written consent of the Parties.<br>
                7. If the Supplier uses the services of Third Parties
                to fulfill its obligations of the Service Order, Company shall not
                have any kind of responsibility in front of that Third Party. In this
                case, the Supplier shall respond completely and directly in front
                of that mentioned Third Party, reimbursing to Company, besides
                and in its case, all the incurred costs and legal fees by
                Company if the Supplier defaults with its at this point stated
                obligation.<br>
                8. If the Supplier develops any kind of document,
                design, graphic, commercials, etc. (the “creations”) trough the
                materialization of the Service Order, these creations, due to that
                the shall be considered works or services requested by
                Company, they shall be and remain in its case, of the exclusive
                property of Company.<br>
                9. Each party and their staff or subcontractors, shall
                to keep in secret and keep confidential all information obtained
                for the execution and performance of this instrument, and agree
                not to use, disclose, sell, assign, negotiate or otherwise transfer<br>
                10. Company may terminate this Service Order at
                without prior written authorization of its counterpart.<br>
                11. The Supplier shall keep harmless and in save
                Company from any legal claim of sue, if it was alleged by any
                third party, that the goods or services object of the Service
                Order, invade intellectual property rights, patents, trademarks,
                copyrights, license or other rights of third parties. The Supplier
                shall be liable for damages caused to Company, for its breach.<br>
                12. The Supplier shall not be allowed to assign or
                transfer all or part of its rights and obligations that have acquire
                by means of thisService Order.<br>
                13. The execution of this agreement shall not be
                deemed to create any kind of relationship between the parties,
                not expressly mentioned herein.<br>
                14. This agreement and the exhibits produced as
                contemplated herein, represents the full understanding between
                the parties in regards to the purpose mentioned above,
                superseded and replaces any and all prior agreements, whether
                oral or written made in an earlier date.Any
                </p>
        </td>
        <td style="width: 50%;">
                <p style="text-align: justify; padding-left: 1mm">
                amendment shall only be valid if made in writing and sign by
                theirrespective legal representatives.<br>
                15. Each party will be responsible for the taxes
                according to the applicable laws it incurs due to the execution
                or performance of this ServiceOrder.<br>
                16. The relationship between the parties is which
                derives from the covenants contained herein and each party
                will be responsible for its own personnel. If the Supplier hires
                any Third Party to produce the Product, such fact will not be
                deemed as if Company has any kind of relationship with it.<br>
                17. No waiver of any term, provision or condition of
                this agreement in any one or more instances, shall be deemed
                to be or be construed as a further or continuing waiver of any
                such term, provision or condition or as a waiver of any other
                term, provision or condition of this Agreement. The rights or
                remedies set forth herein are in addition to any rights or
                remedies which may be granted by law or equity.<br>
                18. This Service Order is not a license or
                assignment of rights of any brand or design, if any, in its case,
                is provided to the Supplier to materialize the services object of
                this ServiceOrder.<br>
                19. The Supplier shall hire an insurance policy
                enough to cover all the payments given in advance for the
                Service Order form Company to the Supplier.<br>
                20. Any communications between the parties will be
                made in writing and delivered to the addresses at the obverse
                mentioned.<br>
                21. The Supplier must have property insurance
                coverage for damage to third parties for covering the
                corresponding value of the damages.<br>
                22. It is understood by the Supplier that Tyson (as
                subsidiary of a North American based Company), is subject to
                the compliance of internal, external and corporate norms. For
                this reason and to the extent applicable, Supplier agrees to
                provide services in keeping with the Code of Conduct
                http://www.tyson.com.mx/docs/TdM-CodCon.pdf TYSON,
                making it also of the knowledge of its staff (related to these
                general conditions).
                        The Supplier and its representatives shall not
                offer, give, promise or authorize to give and will not offer,
                deliver, promise or authorize to give, directly or indirectly, any
                amount of money or valuables to any government employee,
                political party or candidate of public office or political in relation
                to services related underthis contract.
                        The Supplier will make and maintain for a period
                of two years following the expiration date of this document, a
                proper account of its books and records relating to the
                activities, income and expenses related to this document. Not
                more than once a year, a previous notification by Tyson and
                on a reasonable way, Tyson will be responsible through his
                staff or through the use of external auditors to review such
                books and records and other records which prove necessary
                consultation to verify compliance with the terms of this
                agreement and, under Tyson's request, will promptly provide a
                copy of the documents that the Supplier reasonably request to
                validate compliance with the provisions of this paragraph.<br>
                23. Any controversy, dispute or difference among
                the Parties, about the interpretation and fulfillment of this
                document, shall be governed pursuant to the applicable laws
                of the city indicated at the obverse of this page and they shall
                be expressly submitted to the jurisdiction of the competent
                Courts of the aforesaid city (ies), understanding the Parties
                that (those) city (ies) as well, as the accomplishment place(s)
                of the object of this agreement, hereby, waiving the Parties,
                any other jurisdiction that may correspond in lieu of present or
                future domiciles.
                </p>
        </td>
    </tr>
</table>

</page>`
}

module.exports = {
  returnPDF
}
